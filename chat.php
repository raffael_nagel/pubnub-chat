<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="author" content="Raffael Nagel" />
    <title>RN Chat</title>

    <!----------------------------------------------------------------------------->
    <link href="assets/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">

    <link href="assets/plugins/notifications/notification.css" rel="stylesheet">
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="assets/css/menu.css" rel="stylesheet" type="text/css">
    <link href="assets/css/responsive.css" rel="stylesheet" type="text/css">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <!----------------------------------------------------------------------------->


</head>
<body class="fixed-left">

    <!-- Begin page -->
    <div id="wrapper">

        <!-- Top Bar Begin -->
        <div class="topbar">
            <!-- LOGO -->
            <div class="topbar-left">
                <div class="text-center">
                    <a href="#" class="logo"><i class="md md-brightness-7"></i> <span>RN  Chat</span></a>
                </div>
            </div>
            <!-- Button mobile view to collapse sidebar menu -->
            <div class="navbar navbar-default" role="navigation">
                <div class="container">
                    <div class="">
                        <div class="pull-left">
                            <button class="button-menu-mobile open-left">
                                <i class="fa fa-bars"></i>
                            </button>
                            <span class="clearfix"></span>
                        </div>

                        <ul class="nav navbar-nav navbar-right pull-right">
                            <li class="hidden-xs">
                                <a href="#" id="btn-fullscreen" class="waves-effect"><i class="md md-crop-free"></i></a>
                            </li>
                            <li class="hidden-xs">
                                <a href="#" class="right-bar-toggle waves-effect"><i class="md md-chat"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="" class="dropdown-toggle profile" data-toggle="dropdown" aria-expanded="true"><img src="assets/images/avatar-1.jpg" alt="user-img" class="img-circle" id="user-avatar"> </a>
                                <ul class="dropdown-menu">
                                    <li><a id="btn-logout" href="#"><i class="md md-settings-power"></i> Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <!-- Top Bar End -->

        <!-- Left Sidebar Begin -->
        <div class="left side-menu">
            <div class="sidebar-inner slimscrollleft">
                <div class="channels">
                    <h3>Channels</h3>
                </div>
                <!--- Divider -->
                <div id="sidebar-menu">
                    <ul>
                        <li>
                            <a href="#" onclick="" class="waves-effect waves-light active">
                                <i class="md md-bookmark"></i><span> General </span></a>
                        </li>
<!--                        <li>-->
<!--                            <a href="#" onclick="" class="waves-effect waves-light">-->
<!--                                <i class="md md-cloud"></i><span> Random </span></a>-->
<!--                        </li>-->

                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <!-- Left Sidebar End -->


        <!-- Left MainContent Begin -->
        <div class="content-page">
            <div class="content" id="dashboard" data-pagina="dashboard">
                <div class="container">

                    <div class="row">

                        <!-- CHAT -->
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title" id="channelTitle">Channel: <strong>General</strong></h3>
                                </div>
                                <div class="panel-body">
                                    <div class="chat-conversation">
                                        <!-- Messages -->
                                        <ul class="conversation-list nicescroll">

                                        </ul>
                                        <!-- Messages -->


                                    </div>
                                </div>
                            </div>
                        </div> <!-- end col-->

                    </div> <!-- end row -->

                </div> <!-- container -->
            </div> <!-- content -->
            <footer class="footer text-right">
                <div class="col-sm-9 chat-inputbar">
                    <textarea class="form-control chat-input" id="message-input" placeholder="Type your message here ..."></textarea>
                </div>
                <div class="col-sm-3 chat-send">
                    <button type="button" class="btn btn-info btn-lg btn-block waves-effect waves-light">Send</button>
                </div>
            </footer>
        </div>
        <!-- Left MainContent End -->

        <!-- Right Sidebar -->
        <div class="side-bar right-bar nicescroll">
            <h4 class="text-center">Online Users</h4>
            <div class="contact-list nicescroll">
                <!-- Online Users -->
                <ul class="list-group contacts-list">

                </ul>
                <!-- Online Users -->
            </div>
        </div>
        <!-- /Right-bar -->

    </div>


    <script>
        var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/waves.js"></script>
    <script src="assets/js/wow.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/plugins/moment/moment.js"></script>
    <script src="assets/plugins/moment/moment-timezone.js"></script>
    <script src="assets/js/jquery.app.js"></script>


    <script>
        var userAvatar = "<?php echo $_POST['avatar'] ?>";
        var userName   = "<?php echo $_POST['username'] ?>";
    </script>

    <script src="https://cdn.pubnub.com/sdk/javascript/pubnub.4.4.3.js"></script>
    <script src="js/libs/handlebars/handlebars-v4.0.5.js"></script>
    <script src="js/templates/templates.js"></script>
    <script src="js/models/chatModel.js"></script>
    <script src="js/views/chatView.js"></script>
    <script src="js/controllers/chatController.js"></script>
    <script src="js/app.js"></script>




    <script>
        $('.chat-input').keypress(function (ev) {
            var p = ev.which;
            if (p == 13) {
                chatController.sendMessage($('.chat-input').val());
                return false;
            }
        });

        //binding send button click
        $('.chat-send .btn').click(function (ev) {
            chatController.sendMessage($('.chat-input').val());
            return false;
        });

        //binding logout button click
        $('#btn-logout').click(function (ev) {
            chatController.leave();
            return false;
        });
    </script>


</body>
</html>