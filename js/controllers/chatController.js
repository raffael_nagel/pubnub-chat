var chatController = function(model, view){
    this.model = model;
    this.view = view;
};

chatController.prototype = {

    joinChannel: function(channel){
        var self = this;

        this.model.setChannel(channel);

        this.model.chatObj.addListener({
            status: function (statusEvent) {
                if (statusEvent.category === "PNConnectedCategory") {
                    self.setUserId(userName);
                    self.setUserStatus(userName,{"avatar" : userAvatar});
                }
            },
            message: function (messageObj) {
                self.printMessage(messageObj);
            },
            presence: function (presenceObj) {
                self.view.setUserStatus(presenceObj);
            }
        });

        this.model.chatObj.subscribe({
            channels: [channel],
            withPresence: true
        });
    },

    sendMessage: function(message){
        if(message == "")return;
        this.model.publish(message);
    },

    printMessage: function(message){

        var self = this;
        this.model.chatObj.getState({
                uuid: message.publisher,
                channels: [self.model.channel]
            },
            function (status, response) {
                var type = "received";
                if(message.publisher == self.model.getUserId()){
                    type = "sended";
                }
                message.type = type;
                message.userAvatar = response.channels[self.model.channel].avatar;
                self.view.printMessage(message);
            }
        );
    },

    sendSystemMessage: function(message){
        var currentId = this.model.getUserId();
        this.model.setUserId("Admin");
        this.model.publish(message);
        this.model.setUserId(currentId);
    },

    getOldMessages: function(count){
        var self = this;

        count = count || 100;

        this.model.chatObj.history({
                channel: self.model.channel,
                reverse: true,
                count: count,
                stringifiedTimeToken: true
            },
            function (status, response) {
                self.view.printOldMessages(response.messages);
            }
        );
    },

    setUserStatus: function(userId,status){
        var self = this;
        this.model.chatObj.setState({
                uuid: userId,
                state: status,
                channels: [self.model.channel]
            },
            function (status, response) {
                self.setUserAvatar(userId);
                self.getLoggedUsers();
            }
        );
    },

    setUserAvatar: function(userId){
        var self = this;
        this.model.chatObj.getState({
                uuid: userId,
                channels: [self.model.channel]
            },
            function (status, response) {
                self.view.setUserAvatar(response.channels[self.model.channel]);
            }
        );
    },

    getLoggedUsers: function(){
        var self = this;
        this.model.chatObj.hereNow({
                channels: [self.model.channel],
                includeUUIDs: true,
                includeState: true
            },
            function (status, response){
               self.view.setLoggedUsers(response.channels[self.model.channel].occupants);
            }
        );
    },

    leaveChannel: function(){
        this.model.leave();
    },

    setUserId : function(userId){
        this.model.setUserId(userId);
    },

    getUserId: function(){
        return this.model.getUserId();
    }

};