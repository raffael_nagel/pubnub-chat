var chatView = function(chatModel){
    this.model = chatModel;

    this.$body = $("body");
    this.$chatInput = $('.chat-input');
    this.$chatList = $('.conversation-list');
};

chatView.prototype = {

    printMessage : function(message){

        var chatTime = moment(message.timetoken / 10000).local().format("HH:mm");

        var htmlTemplate;
        if(message.type != "sended"){
            htmlTemplate = Templates.message_sended;
        }else{
            htmlTemplate = Templates.message_received;
        }
        var context = { "author" : message.publisher, "message" : message.message, "avatar" : message.userAvatar, "time" : chatTime };

        var template = Handlebars.compile(htmlTemplate);
        var html = template(context);
        $('.conversation-list').append(html);

        this.$chatInput.val('');
        this.$chatInput.focus();
        this.$chatList.scrollTo('100%', '100%', {
            easing: 'swing'
        });

    },

    printOldMessages: function(oldMessages){
        console.log(oldMessages);
    },

    setLoggedUsers: function(users){
        var htmlTemplate;
        htmlTemplate = Templates.users_online;

        var context = { "users" : users};

        var template = Handlebars.compile(htmlTemplate);
        var html = template(context);
        $('.contacts-list').empty();
        $('.contacts-list').append(html);
    },

    setUserStatus: function(user){
        // join, leave, state-change
        if(user.action == "join"){
            var htmlTemplate;
            htmlTemplate = Templates.user_online;
            var context = { "user" : user};
            var template = Handlebars.compile(htmlTemplate);
            var html = template(context);
            $('.contacts-list').append(html);
        }
        if(user.action == "leave"){
            $('#'+user.uuid).remove();
        }
        if(user.action == "state-change"){
            console.log(user);
        }
    },

    setUserAvatar: function(user){
        var avatar = "assets/images/avatar-3.jpg";
        if(user.avatar != undefined){
            avatar = user.avatar;
        }
        $('#user-avatar').attr("src",avatar);
    }

};