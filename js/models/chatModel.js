var chatModel = function(){
    this.publishKey = "";
    this.subscribeKey = "";
    this.chatObj = {};
    this.channel = "";

    this.init();
};

chatModel.prototype = {


    init : function(){
        this.publishKey = "pub-c-c4835a5f-5e99-4d92-a96b-a1d2213e31fb";
        this.subscribeKey = "sub-c-6be59cca-eff6-11e6-a9d5-0619f8945a4f";
        this.chatObj = new PubNub({
            subscribeKey: this.subscribeKey,
            publishKey: this.publishKey,
            logVerbosity: false,
            ssl: true
        });
        return this;
    },

    setChannel : function(channel){
        this.channel = channel;
    },

    getUserId : function(){
        return(this.chatObj.getUUID());
    },

    setUserId : function(newId){
        this.chatObj.setUUID(newId);
    },

    publish : function(message){
        var publishConfig = {
            channel: this.channel,
            message: message
        };
        this.chatObj.publish(publishConfig, function (status, response) {
            return({status: status, response: response});
        })
    },

    leave : function(){
        var self = this;
        this.chatObj.unsubscribe({
            channels: [self.channel]
        })
    }

};