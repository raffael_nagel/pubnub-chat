Templates = {};

Templates.message_sended = "<li class='clearfix'><div class='chat-avatar'>{{#if avatar}}<img src='{{avatar}}' alt=''>{{else}}<img src='assets/images/avatar-3.jpg' alt=''>{{/if}}<i>{{time}}</i></div><div class='conversation-text'><div class='ctext-wrap'><i>{{author}}</i><p>{{message}}</p></div></div></li>";
Templates.message_received = "<li class='clearfix odd'><div class='chat-avatar'>{{#if avatar}}<img src='{{avatar}}' alt=''>{{else}}<img src='assets/images/avatar-3.jpg' alt=''>{{/if}}<i>{{time}}</i></div><div class='conversation-text'><div class='ctext-wrap'><i>{{author}}</i><p>{{message}}</p></div></div></li>";

Templates.users_online = "" +
    "{{#each users as |user userId|}}"+
        "<li class='list-group-item' id='{{user.uuid}}'>" +
            "<a href='#'>" +
                "<div class='avatar'>" +
                    "{{#if user.state.avatar}}<img src='{{user.state.avatar}}' alt=''>{{else}}<img src='assets/images/avatar-3.jpg' alt=''>{{/if}}" +
                "</div>" +
                "<span class='name'>{{user.uuid}}</span>" +
                "<i class='fa fa-circle online'></i>" +
            "</a>" +
            "<span class='clearfix'></span>" +
        "</li>"+
    "{{/each}}";

Templates.user_online = "" +
    "<li class='list-group-item' id='{{user.uuid}}'>" +
    "<a href='#'>" +
    "<div class='avatar'>" +
    "{{#if user.state.avatar}}<img src='{{user.state.avatar}}' alt=''>{{else}}<img src='assets/images/avatar-3.jpg' alt=''>{{/if}}" +
    "</div>" +
    "<span class='name' >{{user.uuid}}</span>" +
    "<i class='fa fa-circle online'></i>" +
    "</a>" +
    "<span class='clearfix'></span>" +
    "</li>";


